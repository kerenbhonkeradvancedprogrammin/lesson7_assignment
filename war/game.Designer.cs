﻿namespace war
{
    partial class game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.myScore = new System.Windows.Forms.Label();
            this.quitBtn = new System.Windows.Forms.Button();
            this.enemyScore = new System.Windows.Forms.Label();
            this.enemyCard = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.enemyCard)).BeginInit();
            this.SuspendLayout();
            // 
            // myScore
            // 
            this.myScore.AutoSize = true;
            this.myScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.myScore.Location = new System.Drawing.Point(12, 12);
            this.myScore.Name = "myScore";
            this.myScore.Size = new System.Drawing.Size(103, 20);
            this.myScore.TabIndex = 0;
            this.myScore.Text = "Your score: 0";
            this.myScore.TextChanged += new System.EventHandler(this.myScore_TextChanged);
            this.myScore.Click += new System.EventHandler(this.myScore_Click);
            // 
            // quitBtn
            // 
            this.quitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quitBtn.Location = new System.Drawing.Point(550, 12);
            this.quitBtn.Name = "quitBtn";
            this.quitBtn.Size = new System.Drawing.Size(100, 37);
            this.quitBtn.TabIndex = 1;
            this.quitBtn.Text = "QUIT";
            this.quitBtn.UseVisualStyleBackColor = true;
            this.quitBtn.Click += new System.EventHandler(this.quitBtn_Click);
            this.quitBtn.KeyUp += new System.Windows.Forms.KeyEventHandler(this.quitBtn_KeyUp);
            this.quitBtn.MouseCaptureChanged += new System.EventHandler(this.Form1_Load);
            this.quitBtn.MouseUp += new System.Windows.Forms.MouseEventHandler(this.quitBtn_MouseUp);
            // 
            // enemyScore
            // 
            this.enemyScore.AutoSize = true;
            this.enemyScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyScore.Location = new System.Drawing.Point(1022, 12);
            this.enemyScore.Name = "enemyScore";
            this.enemyScore.Size = new System.Drawing.Size(151, 20);
            this.enemyScore.TabIndex = 2;
            this.enemyScore.Text = "Opponent\'s score: 0";
            this.enemyScore.Click += new System.EventHandler(this.enemyScore_Click);
            // 
            // enemyCard
            // 
            this.enemyCard.BackgroundImage = global::war.Properties.Resources.card_back_blue;
            this.enemyCard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.enemyCard.Location = new System.Drawing.Point(550, 50);
            this.enemyCard.Name = "enemyCard";
            this.enemyCard.Size = new System.Drawing.Size(100, 114);
            this.enemyCard.TabIndex = 3;
            this.enemyCard.TabStop = false;
            // 
            // game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LimeGreen;
            this.ClientSize = new System.Drawing.Size(1184, 351);
            this.Controls.Add(this.enemyCard);
            this.Controls.Add(this.enemyScore);
            this.Controls.Add(this.quitBtn);
            this.Controls.Add(this.myScore);
            this.Name = "game";
            this.Text = "game";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.game_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.game_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Click += new System.EventHandler(this.game_Click);
            ((System.ComponentModel.ISupportInitialize)(this.enemyCard)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label myScore;
        private System.Windows.Forms.Button quitBtn;
        private System.Windows.Forms.Label enemyScore;
        private System.Windows.Forms.PictureBox enemyCard;
    }
}


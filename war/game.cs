﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace war
{
    public partial class game : Form
    {
        private bool _end;
        private string _myCard;
        private string _enemyCard;
        private int _myTurns;
        private int _enemyTurns;
        private int _myScore;
        private int _enemyScore;

        private PictureBox _card1;
        private PictureBox _card2;
        private PictureBox _card3;
        private PictureBox _card4;
        private PictureBox _card5;
        private PictureBox _card6;
        private PictureBox _card7;
        private PictureBox _card8;
        private PictureBox _card9;
        private PictureBox _card10;
        private NetworkStream _clientStream;

        public game()
        {
            InitializeComponent();
            _myTurns = 0;
            _enemyTurns = 0;
            _myScore = 0;
            _enemyScore = 0;
            _myCard = "";
            _enemyCard = "";
            _end = false;
        }

        private void getCardCode()
        {
            _myCard = "";
            Random rnd = new Random();
            int number = rnd.Next(1, 14);
            int type = rnd.Next(4);

            if(number < 10)
            {
                _myCard = "0";
            }
            _myCard += number;

            switch (type)
            {
                case 0:
                    _myCard += "C";
                    break;
                case 1:
                    _myCard += "D";
                    break;
                case 2:
                    _myCard += "H";
                    break;
                case 3:
                    _myCard += "S";
                    break;
            }
        }

        private void client(object obj)
        {
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            client.Connect(serverEndPoint);
            _clientStream = client.GetStream();

            while (_end == false)
            {
                byte[] bufferIn = new byte[4];
                int bytesRead = _clientStream.Read(bufferIn, 0, 4);
                string input = new ASCIIEncoding().GetString(bufferIn);

                if (input[0] == '0')
                {
                    Invoke((MethodInvoker)delegate { GenerateCards(); quitBtn.Enabled = true; });
                }
                if (input[0] == '1')
                {
                    _enemyTurns++;
                    while (_enemyTurns > _myTurns)
                    {
                        //wait until the user will pick his card
                    }
                    _enemyCard = input.Substring(1);
                    enemyCard.Image = (Bitmap)war.Properties.Resources.ResourceManager.GetObject("_" + _enemyCard);
                    Invoke((MethodInvoker)delegate { updateScores(); });
                }
                if(input[0] == '2')
                {
                    _end = true;
                }
            }

            Application.Exit();
        }

        private void updateScores()
        {
            int myNum = Convert.ToInt32(_myCard.Substring(1, 2));
            int enemyNum = Convert.ToInt32(_enemyCard.Substring(0, 2));
            if(myNum > enemyNum)
            {
                _myScore++;
                myScore.Text = "Your score: " + _myScore;
            }
            else if(enemyNum > myNum)
            {
                _enemyScore++;
                enemyScore.Text = "Opponent's score: " + _enemyScore;
            }
        }

        private void sendMsg ()
        {
            byte[] buffer = new ASCIIEncoding().GetBytes(_myCard);
            _clientStream.Write(buffer, 0, 4);
            _clientStream.Flush();
        }

        private void GenerateCards()
        {
            int space = 17;
            int height = 114;
            int width = 100;
            Point nextLocation = new Point(space, this.Size.Height - 50 - height);

            _card1 = createPic(nextLocation);
            nextLocation.X += width + space;
            _card2 = createPic(nextLocation);
            nextLocation.X += width + space;
            _card3 = createPic(nextLocation);
            nextLocation.X += width + space;
            _card4 = createPic(nextLocation);
            nextLocation.X += width + space;
            _card5 = createPic(nextLocation);
            nextLocation.X += width + space;
            _card6 = createPic(nextLocation);
            nextLocation.X += width + space;
            _card7 = createPic(nextLocation);
            nextLocation.X += width + space;
            _card8 = createPic(nextLocation);
            nextLocation.X += width + space;
            _card9 = createPic(nextLocation);
            nextLocation.X += width + space;
            _card10 = createPic(nextLocation);
        }

        private PictureBox createPic(Point nextLocation)
        {
            // create the image object itself
            System.Windows.Forms.PictureBox currentPic = new PictureBox();
            currentPic.Image = global::war.Properties.Resources.card_back_red;
            currentPic.Location = nextLocation;
            currentPic.Size = new System.Drawing.Size(100, 114);
            currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

            // assign an event to it
            currentPic.Click += delegate (object sender1, EventArgs e1)
            {
                enemyCard.BackgroundImage = global::war.Properties.Resources.card_back_blue;
                _card1.Image = global::war.Properties.Resources.card_back_red;
                _card2.Image = global::war.Properties.Resources.card_back_red;
                _card3.Image = global::war.Properties.Resources.card_back_red;
                _card4.Image = global::war.Properties.Resources.card_back_red;
                _card5.Image = global::war.Properties.Resources.card_back_red;
                _card6.Image = global::war.Properties.Resources.card_back_red;
                _card7.Image = global::war.Properties.Resources.card_back_red;
                _card8.Image = global::war.Properties.Resources.card_back_red;
                _card9.Image = global::war.Properties.Resources.card_back_red;
                _card10.Image = global::war.Properties.Resources.card_back_red;

                getCardCode();
                currentPic.Image = (Bitmap)war.Properties.Resources.ResourceManager.GetObject("_" +_myCard);
                _myCard = "1" + _myCard;
                _myTurns++;
                sendMsg();
            };

            // add the picture object to the form (otherwise it won't be seen)
            this.Controls.Add(currentPic);
            return currentPic;
        }

        private void exit()
        {
            _end = true;
            _myCard = "2000";
            sendMsg();
            MessageBox.Show(myScore.Text + "\n" + enemyScore.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            quitBtn.Enabled = false;
            Thread myThread = new Thread(client);
            myThread.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void quitBtn_KeyUp(object sender, KeyEventArgs e)
        {
        }

        private void quitBtn_MouseUp(object sender, MouseEventArgs e)
        {
        }

        private void myScore_Click(object sender, EventArgs e)
        {

        }

        private void quitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void enemyScore_Click(object sender, EventArgs e)
        {

        }

        private void myScore_TextChanged(object sender, EventArgs e)
        {

        }

        private void game_Click(object sender, EventArgs e)
        {
        }

        private void game_FormClosing(object sender, FormClosingEventArgs e)
        {
            exit();
        }

        private void game_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

    }
}
